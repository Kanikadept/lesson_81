const express = require('express');
const cors = require("cors");
const mongoose = require('mongoose');
///////////////////////////////////////
const shortenUrl = require('./app/shortenUrl');

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/', shortenUrl);

const run = async () => {
    await mongoose.connect('mongodb://localhost/shop', {useNewUrlParser: true, useUnifiedTopology: true});

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}

run().catch(console.error);