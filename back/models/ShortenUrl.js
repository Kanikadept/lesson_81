const mongoose = require('mongoose');

const ShortenUrlSchema = new mongoose.Schema({
    shortenUrl: {
        type: String,
        required: true,
    },
    originalUrl: {
        type: String,
        required: true,
    }
})

const ShortenUrl = mongoose.model('ShortenUrl', ShortenUrlSchema);
module.exports = ShortenUrl;