const express = require('express');
const {nanoid} = require('nanoid');
const ShortenUrl = require('../models/ShortenUrl');

const router = express.Router();

router.get('/:id', async (req, res) => {
   try {
      const shortenUrl = await ShortenUrl.findOne({shortenUrl: req.params.id});

      if (shortenUrl) {
          res.status(301).redirect(shortenUrl.originalUrl);
      } else {
          res.sendStatus(404);
      }
   } catch (e) {
     res.sendStatus(500);
   }
});


router.post('/links', async (req, res) => {
    try {
        const shortenUrlData = req.body;
        if (shortenUrlData.originalUrl) {
            shortenUrlData.shortenUrl = nanoid(6);
            const shortenUrl = new ShortenUrl(shortenUrlData);
            await shortenUrl.save();
            res.send(shortenUrl);
        }

        res.status(400).send({"Error": "original url can't be empty"});
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;