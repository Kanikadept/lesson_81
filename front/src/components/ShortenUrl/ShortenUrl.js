import React, {useState} from 'react';
import './ShortenUrl.css';
import {createShortUrl} from "../../store/actions";
import {useDispatch, useSelector} from "react-redux";

const ShortenUrl = () => {

    const dispatch = useDispatch();
    const shortenUrl = useSelector(state => state.shortenUrl);

    const [url, setUrl] = useState({
        originalUrl: '',
    });

    const handleChange = event => {
        const {name, value} = event.target;
        const urlCopy = {...url};
        urlCopy[name] = value;
        setUrl(urlCopy);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(createShortUrl(url));
    }

    const shortUrlResult = (
        <div className="shorten-url__result-box">
            <h5 className="shorten-url__result-title">Your link now looks liike this:</h5>
            {shortenUrl &&
            <a href={'http://localhost:8000/' + shortenUrl.shortenUrl}
                  className="shorten-url__result">{'http://localhost:8000/' + shortenUrl.shortenUrl}
            </a>}
        </div>
    )

    return (
        <>
            <form onSubmit={handleSubmit} className="shorten-url">
                <h5 className="shorten-url__title">Shorten your link!</h5>
                <input onChange={handleChange}
                       name="originalUrl"
                       value={url.originalUrl}
                       className="shorten-url__input"
                       type="text"/>
                <button className="shorten-url__btn">Shorten!</button>
            </form>
            {shortenUrl && shortUrlResult}
        </>
    );
};

export default ShortenUrl;