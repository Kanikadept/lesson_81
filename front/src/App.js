
import './App.css';
import ShortenUrl from "./components/ShortenUrl/ShortenUrl";

const App = () => (
    <div className="App">
        <ShortenUrl />
    </div>
);

export default App;
