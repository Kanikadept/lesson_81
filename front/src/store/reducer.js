import {CREATE_SHORT_URL} from "./actions";

const initialState = {
    shortenUrl: '',
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_SHORT_URL:
            return {...state, shortenUrl: action.payload};
        default:
            return state;
    }
}

export default reducer;