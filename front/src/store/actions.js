import axiosApi from "../axiosApi";

export const CREATE_SHORT_URL = 'CREATE_SHORT_URL';
export const createShortUrlSuccess = payload => ({type: CREATE_SHORT_URL, payload});
export const createShortUrl = url => {
    return async dispatch => {
        try {
            const shortUrlResponse = await axiosApi.post('/links', url);
            dispatch(createShortUrlSuccess(shortUrlResponse.data));
        } catch (e) {
            console.log(e);
        }
    }
}